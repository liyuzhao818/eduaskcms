# eduaskcms
EduaskCMS是基于ThinkPHP5.0.14框架开发的一个免费开源的内容管理系统，开发的理念不光注重后台，同样注重后台功能的扩展；更加注重的是前台的快速开发和便捷。系统希望实现的目标是“让你的代码，能省则省”。你可以使用EduaskCMS直接进行你自己的建站、学习、功能开发，但不可以在系统基础上更新发布子版系统。
<br />
<br />
<br />
系统网站：http://www.eduaskcms.xin<br />
后台测试：http://www.eduaskcms.xin/run<br />
用户名：test123<br />
密　码：test123<br />

<p>
下载后，将eduaskcms-1.x.x 和libs文件夹直接放到服务器根目录，其中eduaskcms-1.x.x是项目文件夹，libs是核心引入文件夹
</p>
<p>
首页先修改数据库配置文件：eduaskcms-1.x.x/app/database.php，修改里面的 “database数据库名”、“username用户名”、“password密码”、“prefix前缀”。
</p>
<p>域名或虚拟主机安装访问(一定是绑定到public)：域名/install<br /></p>
<p>本地安装可能是：127.0.0.1/eduaskcms/public/install<br /></p>

<p>安装成功以后/run访问后台，默认用户名和密码是：eduask</p>
<p>
培训手册：https://www.kancloud.cn/laowu199/e_manual/ 
</p>
<p>
建站手册：https://www.kancloud.cn/laowu199/e_use/ 
</p>
<p>
开发手册：https://www.kancloud.cn/laowu199/e_dev 
</p>
<p>由于之前版本的一些非法行为和一些不愉快，系统决定从V1.0.5版本开始对几个核心文件进行加密（对安装、使用、开发和部署没有任何影响，这里也保证没有故意留后门代码）。不加密开放版本对 开发手册购买用户提供免费下载服务，你可以登录看云平台查看系统开发手册提示下载（同时也希望购买用户尊重自己的权益和系统权益不要随意在公共网络地址发放系统）。</p>
<p><a href="https://www.kancloud.cn/laowu199/e_dev/446299">下载不加密版本</a></p>

